# CV of William Ku

![me](images/kucheech.jpg)

| 91788673 | kucheech@gmail.com | [LinkedIn profile](https://www.linkedin.com/in/kucheech/) |


## Software developer and IT lecturer
---

## Key achievements and skills
*	Competent in programming such as in Javascript, C/C++C#, Java, Python ```&& …aFewOthers```;
*	Work daily with ![](images/aws.png) ![](images/react.png) ![](images/nodejs.png)
*	Experienced IT lecturer, ACTA-certified
*	Published eight international research conference papers

## Experience
Senior Electronic Access & Data Engineer (Feb 2019 – present)\
*Dormakaba*
*	Full-stack developer (AWS, React, API)
*	Current projects are smarthome and entrance systems IoT connector.
*	System integration and support of access control solutions (exos, exivo and MATRIX)

Full-stack Software Developer (Oct 2017 – Feb 2019)\
*Maltem Asia Pte Ltd*
* Contractor at SGX as frontend javascript developer (project: sgx.com)
*	Internal app development and maintenance on the MEAN stack (Angular2 and PostgreSQL).

Senior Software Developer (Dec 2016 – Aug 2017)\
*Servcorp*
*	ScrumMaster/full-stack developer, working with a distributed team. Product Owner is based in Australia.
*	Backend (PHP, MySQL, RHEL7) and mobile apps (iOS and Android) development, testing and deployment, for enterprise apps in the serviced office domain. Technologies involved: Cisco Wireless Controller, HP Aruba GuestPass and beacons. 
*	Training and demo to internal staff and customers.
*	Technical documentation (End-User and Admin-User guides) / JIRA

Senior Software Developer (Sep 2015 – Dec 2016)\
*Jetmobile Pte Ltd / Celiveo Pte Ltd*
*	Native mobile apps development mainly iOS/Swift and Android/Java. Primarily internal apps for enterprise secure printing.
*	MFP printer solutions development in Android/Java and Linux/C++. Developed an Android app for a particular printer manufacturer.
*	Working with distributed Scrum teams consisting members in France, India and Canada. Product Owner is based in France.
*	Practicing Scrum as a developer.
	
Software Engineer (Feb 2014 – Jun 2015)\
*Trident Technologies Pte Ltd* 
*	Mostly cross platform mobile apps development using two approaches namely Telerik/Cordova/KendoUI/Javascript/JQuery/JQueryMobile and Xamarin/C#. Main lead throughout in mobile app application full developmental cycle from design, development to deployment.
*	Used several Mobile Backend As A Service (mBaaS) namely Microsoft Azure Mobile Services, Parse, AWS and Telerik Platform. Developed web apis for cloud-based data storage using .NET.
*	Apps developed include MyCondo and GeBIZ RSS Viewer (available on Google Play). The MyCondo app assists condo residents to communicate with their condo manager/MCST and to submit photos supporting their issues. It is built on the Xamarin platform (C#/.Net) using Microsoft Azure Mobile Services as the backend. The GeBIZ RSS Viewer is an unofficial mobile app to purview GeBIZ listings and awards. It is built on Cordova/Javascript and using the Telerik Platform as the backend.  
*	Also did some native web/mobile apps development for internal use in both iOS(Objective-C/Swift) and Android. This includes a digital signature extension to the company’s flagship products, allowing users to input and apply their signature (for some healthcare-related authorization). 
*	Assisted in technical support for company flagship products in ASP.NET such as UAT and SIT.
*	Pre-sales consultant for customized software solutions. Active participation and bidding on GeBIZ/Singhealth GPO Ariba/Sesami Tender/RFQ listings. 
Mobile app developer (Jun 2010 – Feb 2014)
appable
*	Developed mobile apps for consumers and corporate users. All native mobile apps development (Objective-C/iOS, Java/Android and C#/Windows Phone). Some apps provide ads-revenue. Reference app is Building Doctor (available on Google Play) for Aegis Building & Engineering Pte Ltd (facility maintenance contractor) which allows users to file incident reports/request for quotation using photos and voice memos. The app will email the reports and requests to the designated staff for follow-up. 
*	Published apps on iOS, Android and Windows Phone platforms.
*	Managed a few social media accounts for SMEs.

## Teaching
MDIS, Associate Lecturer (Sep 2015 – present)\
Currently teaching Teesside University CIS3004 Computing Project

NTUC Learning Hub, Associate Trainer (Aug 2017 – present)\
Currently teaching MTA Software development fundamentals

Auston Institute of Management, Associate Lecturer (Jul 2015 – present)\
Last taught De Montfort University CTEC2907 Web Application Development and CTEC3110 Secure Web Application Development.

Kaplan, Associate Lecturer (Jan 2012 – Jul 2019)\
Teaching several diploma/degree bridging IT modules including a mobile app development module. 
Also developed course content.

Amity Global Institute, Lecturer (Mar 2019 – Jun 2019)\
Teaching UON CSY1018 - Web Development,  CSY1019 - Software Engineering 1, CSY2028-Web Programming and CSY2030 – System Design and Development.

TMC Academy, Lecturer (Nov 2015 – Dec 2018)\
Teaching University of Greenwich computer science project and mobile apps (Android/Phonegap) modules.

Temasek Polytechnic, Adjunct Lecturer (Apr 2011 – Oct 2012)\
Taught several diploma IT modules, mostly C#/Java programming and Flash/AS3.

National University of Singapore, Teaching Assistant (Jan 2007 – Jun 2010)\
Taught undergraduate computer science modules, mostly C programming and computer architecture.

## Certification
* Foundations in AI\
AI Singapore

* AWS Certified Cloud Practitioner (valid till Sep 2022)

* NICF - Secure Software Development Lifecycle for Agile (Nov 2018)\
*Singapore Workforce Skills Qualifications System*

* PCP for FullStack Software Developer (Aug 2017 – Apr 2018)\
*Singapore Workforce Skills Qualifications System*

* Certified Scrum Master (valid till May 2020)\
Certified Scrum Developer (valid till May 2020)\
*Scrum Alliance*

* NICF- (ISC)² CISSP CBK Training Seminar (Mar 2017)\
NICF-Object Oriented Design Patterns (Jul 2016)\
NICF-Software Testing (Jun 2016)\
NICF-Certified ScrumMaster (Apr 2016)\
NICF-Essential Practices for Agile Teams (Feb 2016)\
NICF- Design and Develop Mobile Enterprise Application with HTML5 (Mar 2015)\
NICF - Enterprise Social Business Bootcamp - Leveraging Social for Business Outcomes (Feb 2013)\
Advanced Certificate in Training and Assessment (Facilitated Learning) (Apr 2013)\
*Singapore Workforce Skills Qualifications System*

## Education
Pursuing PhD (Computing), National University of Singapore\
A*STAR Graduate Fellowship\
Published eight research papers\
Did not complete, stopped in Jan 2007.

Master of Science (Computing), National University of Singapore (Jan 2003)\
NUS Research Scholarship + NSTB AMP Award

BEng (Computer Engineering) with Minor in Management Information Systems, National University of Singapore (Jul 2001)\
Honours 2nd Class Upper
